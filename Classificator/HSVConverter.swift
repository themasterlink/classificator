//
//  HSVConverter.swift
//  Classificator
//
//  Created by Max on 18.01.16.
//  Copyright © 2016 Maximilian Denninger. All rights reserved.
//

import Foundation
import Cocoa

class HSVConverter {
    
    var color : NSColor;
    
    init(h : Double, s : Double = 1.0, v : Double  = 1.0){
        color = NSColor(calibratedHue: CGFloat(h), saturation: CGFloat(s), brightness: CGFloat(v), alpha: 1.0);
    }
    
}