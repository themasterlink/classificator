//
//  ClassDataPoint.swift
//  Classificator
//
//  Created by Max on 18.01.16.
//  Copyright © 2016 Maximilian Denninger. All rights reserved.
//

import Foundation

func *(left : Double, right : ClassDataPoint) -> ClassDataPoint{
    let res = ClassDataPoint(dim: right.dim, classNr: right.classNr);
    for (i,val) in right.values.enumerate() {
        res[i] = (left * val);
    }
    return res;
}

func +(left : ClassDataPoint, right : ClassDataPoint) -> ClassDataPoint{
    if left.classNr != right.classNr {
        printError("The addition of two class data points works only if they have the same class!");
    }
    let res = ClassDataPoint(dim: left.dim, classNr: left.classNr);
    if left.dim == right.dim {
        for (index, val) in left.values.enumerate() {
            res[index] = val + right[index];
        }
    }
    return res;
}

class ClassDataPoint : DataPoint {
    
    var classNr : Int;
    
    init(dim: Int, classNr: Int){
        self.classNr = classNr;
        super.init(dim: dim);
    }
    
    init(oldPoint : DataPoint, classNr : Int){
        self.classNr = classNr
        super.init(dim: oldPoint.dim);
        for i in 0 ..< dim {
            values[i] = oldPoint.values[i];
        }
    }
    
    override var description : String{
        get{
            return "\(values) - \(classNr)";
        }
    }
    
    override func getCopy() -> ClassDataPoint {
        return ClassDataPoint(oldPoint: super.getCopy(), classNr: classNr);
    }
}

class ClassDataPoints : DataPoints {
    
    var m_classes : [Int] = [];
    
    var m_nrOfClass : Int {
        get{
            return m_classes.count;
        }
    }
    
    override func append(data: DataPoint) {
        var actData : ClassDataPoint;
        if !(data is ClassDataPoint) {
            actData = ClassDataPoint(oldPoint: data, classNr: 0);
        }else{
            actData = data as! ClassDataPoint;
        }
        addClass(actData.classNr);
        points.append(actData);
    }
    
    func addClass(classNr : Int){
        for c in m_classes {
            if c == classNr {
                return;
            }
        }
        m_classes.append(classNr);
    }
    
    func meanVar() -> [(ClassDataPoint, Var: (X : Double, Y : Double))] {
        var res : [(ClassDataPoint, Var: (X : Double, Y : Double))] = []
        var counter : [Int : Int] = [:];
        for p1 in points {
            let p = p1 as! ClassDataPoint
            var foundClass = false;
            for var r in res {
                if r.0.classNr == p.classNr {
                    foundClass = true;
                    let fac = Double(counter[p.classNr]!) / Double(counter[p.classNr]! + 1);
                    r.0 = (1.0 - fac) * r.0 + fac * p;
                    ++(counter[p.classNr]!);
                    break;
                }
            }
            if !foundClass {
                res.append((p, (0.0,0.0)));
                counter[p.classNr] = 1;
            }
        }
        return res;
    }
    
    override var description : String {
        get{
            var res = "[";
            for ele in points {
                res += "\(ele as! ClassDataPoint), ";
            }
            res += "]"
            return res;
        }
    }
    
}