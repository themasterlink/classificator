//
//  AppDelegate.swift
//  Classificator
//
//  Created by Max on 16.01.16.
//  Copyright © 2016 Maximilian Denninger. All rights reserved.
//

import Cocoa
import Foundation
import SceneKit

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    @IBOutlet weak var m_window: MyWindow!

    
    @IBOutlet weak var m_view: GraphicView!
    
    func applicationDidFinishLaunching(aNotification: NSNotification) {
        
//            NSColor.whiteColor().setStroke()
//            window.contentView!.drawRect(NSRect(x: 0, y: 0, width: 0, height: 0))
        m_window.m_graphicView = m_view;
        print("Start");
        
        
        let points = DataPointReader(filePath: "/Users/Max/Documents/XCode_Projects/Bitbucket/Classificator/Classificator/TestData/testInput21A.txt", isTrainData: false).dataPoints;
//        print("\(points)")
        let classPoints = ClassDataPoints();
        classPoints.append(ClassDataPoint(dim: 2, classNr: 0));
        classPoints.points.last!.values[0] = -1.0;
        classPoints.points.last!.values[1] = -1.0;
        classPoints.append(ClassDataPoint(dim: 2, classNr: 1));
        classPoints.points.last!.values[0] = 3.0;
        classPoints.points.last!.values[1] = 3.0;
        classPoints.append(ClassDataPoint(dim: 2, classNr: 2));
        classPoints.points.last!.values[0] = 1.0;
        classPoints.points.last!.values[1] = -3.0;
        
        let nearestNeighborClass = NeartestNeighbors(points: classPoints);
        
        let p2 = nearestNeighborClass.test(withData: points).meanVar();
        
      
        print("\(p2)")
        
        
        print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n")
        m_view.drawContentMultiDim(nearestNeighborClass.test(withData: points), dims: [0,1]);
        m_view.drawContentMultiDim(classPoints, dims: [0,1], special: true);
        
        var newClassP = ClassDataPoints()
        for pl in p2 {
            print("\(pl.0)")
            newClassP.append(pl.0)
        }
        m_view.drawContentMultiDim(newClassP, dims: [0,1], special: true);
        
        
//        print("\(points)");
        m_view.needsDisplay = true
//        print("\(points.mean())");
        // Insert code here to initialize your application
        print("Init end")
        
        m_window.setFrame(m_window.frame, display: true);
    }

    @IBAction func pushRefreshButton(sender: NSButton) {
        let rec = NSMakeRect(0, 0, m_window.frame.width, m_window.frame.height);
        m_view.frame = rec;
        m_window.update()
    }
    
    
    func applicationWillTerminate(aNotification: NSNotification) {
        // Insert code here to tear down your application
    }


}

