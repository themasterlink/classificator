//
//  MyWindow.swift
//  Classificator
//
//  Created by Maximilian Denninger on 18.01.16.
//  Copyright © 2016 Maximilian Denninger. All rights reserved.
//

import Foundation
import Cocoa

class MyWindow : NSWindow {
    
    var m_graphicView : GraphicView? = nil;
    
    @IBOutlet weak var m_refreshButton: NSButton!
    
    override func setFrame(frameRect: NSRect, display flag: Bool) {
        var newRec = NSMakeRect(frameRect.origin.x, frameRect.origin.y, max(400,frameRect.width), max(300,frameRect.height));
        super.setFrame(newRec, display: flag)
        if m_graphicView != nil {
            m_graphicView!.frame = NSMakeRect(0,0,newRec.width, newRec.height);
            m_refreshButton.frame.origin = CGPointMake(max(400,frameRect.width) / 2.0 - m_refreshButton.frame.width / 2, 5);
        }
    }
}