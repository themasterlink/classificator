//
//  GraphicView.swift
//  Classificator
//
//  Created by Max on 17.01.16.
//  Copyright © 2016 Maximilian Denninger. All rights reserved.
//

import Foundation
import Cocoa

typealias PointWithColor = (val: (x: Double, y: Double), color : Double, special : Bool)

var minMax : (Min : DataPoint, Max : DataPoint)? = nil;

class GraphicView : NSView {
    
    
    func drawContentMultiDim(points : DataPoints, var dims : [Int], special : Bool = false){
        if points.count > 0 && dims.count > 1 {
            if minMax == nil {
                minMax = points.getMinMax();
            }
            let dist = minMax!.Max - minMax!.Min;
            var p : PointWithColor = ((0.0, 0.0), -1, true);
            for point in points.points {
                p.val.0 = (point.values[dims[0]] - minMax!.Min[dims[0]]) / dist[dims[0]] * 0.8 + 0.1;
                p.val.1 = (point.values[dims[1]] - minMax!.Min[dims[1]]) / dist[dims[1]] * 0.8 + 0.1;
                p.special = special;
                draw2DPoint(p);
            }
        }
    }
    
    func drawContentMultiDim(points : ClassDataPoints, var dims : [Int], special : Bool = false){
        if points.count > 0 && dims.count > 1 {
            if minMax == nil {
                minMax = points.getMinMax();
            }
//            Swift.print("Min: \(minMax)")
            let dist = minMax!.Max - minMax!.Min;
            var p : PointWithColor = ((0.0, 0.0), 0, false);
            for point  in points.points {
                let realPoint = point as! ClassDataPoint;
                p.val.0 = (realPoint.values[dims[0]] - minMax!.Min[dims[0]]) / dist[dims[0]] * 0.8 + 0.1;
                p.val.1 = (realPoint.values[dims[1]] - minMax!.Min[dims[1]]) / dist[dims[1]] * 0.8 + 0.1;
                p.color = Double(realPoint.classNr) / Double(points.m_nrOfClass);
                p.special = special;
                draw2DPoint(p);
            }
        }
    }
    
    func draw2DPoint(p : PointWithColor){
//        Swift.print("P: \(p)");
        m_point.append(p);
//        Swift.print("\(m_point.count)")
//        drawRect(NSRect(x: 0, y: 0, width: 480, height: 360))
    }

    var m_point : [PointWithColor] = [];
    
    var m_size : CGFloat = 100.0;
    
    var m_functionInDrawCall : () -> Void = {};
    
    override func drawRect(dirtyRect: NSRect) {
        super.drawRect(dirtyRect)
        Swift.print("Draw rect with: \(dirtyRect)");
//        let bPath : NSBezierPath = NSBezierPath(rect: dirtyRect)
//        let fillColor = NSColor(red: 1.0, green: 0.0, blue: 1.0, alpha: 1.0)
//        fillColor.set()
//        bPath.fill()
        
//        let borderColor = NSColor(red: 1.0, green: 0.0, blue: 0.0, alpha: 1.0)
//        borderColor.set()
//        bPath.lineWidth = 12.0
//        bPath.stroke()
//      
        let newIntern = NSMakeRect(dirtyRect.origin.x + 10, dirtyRect.origin.y + 45, max(0,dirtyRect.size.width - 20) , max(0,dirtyRect.size.height - 80));
        let size = max(newIntern.size.height, newIntern.size.width);
        let newSize = size / m_size;
        // paint white background
        let backGround = NSBezierPath(rect: newIntern)
        NSColor(deviceWhite: 1.0, alpha: 1.0).set()
        backGround.fill();
        // paint black border
        let backGroundLine = NSBezierPath(rect: newIntern)
        backGroundLine.lineWidth = 2
        backGroundLine.stroke()
        
//        let cPath = NSGradient(startingColor: HSVConverter(h: 1.0).color, endingColor: NSColor(deviceWhite: 1, alpha: 1));
//        cPath!.drawFromCenter(newIntern.origin, radius: 1, toCenter: newIntern.origin, radius: 20, options: NSGradientDrawsBeforeStartingLocation);
       
        
        for p in m_point {
            let circleFillColor = HSVConverter(h: p.color).color;
//            Swift.print("width: \(dirtyRect.size.width) -> \(dirtyRect.size.width * CGFloat(p.val.0))")
//            Swift.print("height: \(dirtyRect.size.height) -> \(dirtyRect.size.height * CGFloat(p.val.1))")
            let circleRect = NSMakeRect(newIntern.size.width * CGFloat(p.val.0) + newIntern.origin.x, newIntern.size.height  * CGFloat(p.val.1) + newIntern.origin.y, newSize, newSize)
            let cPath: NSBezierPath = NSBezierPath(ovalInRect: circleRect)
            circleFillColor.set()
            cPath.fill()
//            let cPath = NSGradient(startingColor: circleFillColor, endingColor: NSColor(deviceWhite: 1.0, alpha: 1.0))
//            cPath?.drawFromCenter(circleRect.origin, radius: 0.2, toCenter: circleRect.origin, radius: 2, options: NSGradientDrawsBeforeStartingLocation)
            if p.special {
//                Swift.print("width: \(dirtyRect.size.width) -> \(dirtyRect.size.width * CGFloat(p.val.0))")
//                Swift.print("height: \(dirtyRect.size.height) -> \(dirtyRect.size.height * CGFloat(p.val.1))")
//                let circleRect = NSMakeRect(dirtyRect.size.width * CGFloat(p.val.0), dirtyRect.size.height  * CGFloat(p.val.1), dirtyRect.size.width/100, dirtyRect.size.width/100)
                let newMiddle = NSMakeRect(
                    newIntern.size.width  * CGFloat(p.val.0) + size/(3.3333333333 * m_size) + newIntern.origin.x,
                    newIntern.size.height * CGFloat(p.val.1) + size/(3.3333333333 * m_size) + newIntern.origin.y
                    , size/(2.5 * m_size), size/(2.5 * m_size))
                let bPath: NSBezierPath = NSBezierPath(ovalInRect: newMiddle)
//                Swift.print("Color: \(p.color) \(fmod(Double(p.color + 1.0), 1.0))")
//                HSVConverter(h: fmod(Double(p.color + 0.75), 1.0)).color.set()
                HSVConverter(h: fmod(Double(p.color + 0.5), 1.0)).color.set()
//                bPath.lineWidth = dirtyRect.size.width/500
//                bPath.stroke()
                bPath.fill()
                
                
                
//                let aPath = NSBezierPath(ovalInRect: circleRect);
//                NSColor(deviceRed: 0, green: 0, blue: 0, alpha: 1.0).set()
//                aPath.lineWidth = dirtyRect.size.width/600
//                aPath.stroke()
            }
        }
    }
    
}