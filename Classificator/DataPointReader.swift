//
//  DataPointReader.swift
//  Classificator
//
//  Created by Max on 18.01.16.
//  Copyright © 2016 Maximilian Denninger. All rights reserved.
//

import Foundation


class DataPointReader {
    
    var dataPoints : DataPoints;
    
    init(filePath : String, isTrainData : Bool = true){
        var text = "";
        dataPoints = DataPoints();
        do {
            text = try String(contentsOfFile: filePath, encoding: NSUTF8StringEncoding);
        }catch let error as NSError {
            print("Reading of the file failed with error: \(error)");
            return;
        }
        //println("text: \(text)");
        let lines = text.componentsSeparatedByString("\n");
        let seperaterSymbols = ",; \t";
        var sepSym = "";
        for c in lines[0].characters {
            if seperaterSymbols.characters.contains(c) {
                sepSym.append(c);
            }
        }
        if sepSym == "" {
            printError("First line does not contain any known seperator!")
            return;
        }
        for line in lines {
            if line.lengthOfBytesUsingEncoding(line.fastestEncoding) > 1 {
                let elements = line.componentsSeparatedByString(sepSym);
                
                if !isTrainData {
                    let newDataPoint = DataPoint(dim: 0)
                    for ele in elements {
                        let d = Double(ele);
                        if d != nil {
                            newDataPoint.values.append(d!);
                        }
                    }
                    dataPoints.append(newDataPoint);
                }else{
                    let newDataPoint = ClassDataPoint(dim: 0, classNr: 0);
                    for i in 0 ..< elements.count - 1{
//                        print("\(elements[i])")
                        let d = Double(elements[i]);
                        if d != nil {
                            newDataPoint.values.append(d!);
                        }
                    }
                    let classNr = Int(elements.last!);
                    if classNr != nil {
                        newDataPoint.classNr = classNr!;
                    }
                    dataPoints.append(newDataPoint);
                }
            }
        }
    }

    
}