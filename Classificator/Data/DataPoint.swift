//
//  DataPoint.swift
//  Classificator
//
//  Created by Max on 16.01.16.
//  Copyright © 2016 Maximilian Denninger. All rights reserved.
//

import Foundation


func +=(inout left: DataPoint, right: DataPoint){
    if left.dim == right.dim {
        for index in 0 ..< left.dim {
            left[index] += right[index];
        }
    }
}

func *(left : Double, right : DataPoint) -> DataPoint{
    let res = DataPoint(dim: right.dim);
    for (i,val) in right.values.enumerate() {
        res[i] = (left * val);
    }
    return res;
}

func +(left : DataPoint, right : DataPoint) -> DataPoint{
    let res = DataPoint(dim: left.dim);
    if left.dim == right.dim {
        for (index, val) in left.values.enumerate() {
            res[index] = val + right[index];
        }
    }
    return res;
}

func -(left : DataPoint, right : DataPoint) -> DataPoint{
    let res = DataPoint(dim: left.dim);
    if left.dim == right.dim {
        for (index, val) in left.values.enumerate() {
            res[index] = val - right[index];
        }
    }
    return res;
}

class DataPoint : CustomStringConvertible {
    
    var values : [Double] = [];
    
    var dim : Int {
        get{
            return values.count;
        }
    }
    
    init(dim : Int){
        values = [Double](count: dim, repeatedValue: 0.0);
    }
    
    subscript(i : Int) -> Double {
        get{
            return values[i];
        }
        set(val){
            values[i] = val;
        }
    }
    
    var description : String {
        get{
            return "\(values)";
        }
    }
    
    func getEucDist(to point : DataPoint) -> Double {
        var res = 0.0;
        for i in 0 ..< dim {
            let diff = values[i] - point[i];
            res += diff * diff;
        }
        return res;
    }
    
    func getEucDistSqrt(to point : DataPoint) -> Double {
        return sqrt(getEucDist(to: point))
    }
    
    func getManhattenDist(to point : DataPoint) -> Double {
        var res = 0.0;
        for i in 0 ..< dim {
            res += fabs(values[i] - point[i]);
        }
        return res;
    }
    
    func getCopy() -> DataPoint {
        let ret = DataPoint(dim: dim);
        for i in 0 ..< dim {
            ret.values[i] = values[i];
        }
        return ret;
    }
}

class DataPoints : CustomStringConvertible {
    
    var points : [DataPoint] = [];
    
    var count : Int {
        get{
            return points.count;
        }
    }
    
    var dim : Int {
        get{
            if count > 0 {
                return points[0].dim;
            }
            return 0;
        }
    }
    
    func append(data : DataPoint) {
        points.append(data);
    }
    
    func mean() -> DataPoint {
        if count > 0 {
            var counter = 0
            var res = DataPoint(dim: dim);
            for point in points {
                let fac = 1.0 / Double(counter + 1);
                res = (1.0 - fac) * res + fac * point;
                ++counter;
            }
            return res;
        }
        return DataPoint(dim: 0);
    }
    
    func getMinMax() -> (Min : DataPoint, Max : DataPoint)? {
        if count > 0 {
            let res : (Min : DataPoint, Max : DataPoint) = (DataPoint(dim: dim), DataPoint(dim: dim))
            for i in 0 ..< dim {
                res.Max[i] = -DBL_MAX;
                res.Min[i] = DBL_MAX;
            }
            for p in points {
                for d in 0 ..< dim {
                    if p[d] < res.Min[d] {
                        res.Min[d] = p[d]
                    }
                    if p[d] > res.Max[d] {
                        res.Max[d] = p[d]
                    }
                }
            }
            return res;
        }else{
            return nil;
        }
    }
    
    var description : String {
        get{
            return "\(points)";
        }
    }

    subscript(i : Int) -> DataPoint {
        get{
            return points[i];
        }
        set(val){
            points[i] = val;
        }
    }
}