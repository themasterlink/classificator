//
//  Neartest_Neighbors.swift
//  Classificator
//
//  Created by Max on 18.01.16.
//  Copyright © 2016 Maximilian Denninger. All rights reserved.
//

import Foundation

class NeartestNeighbors {

    var m_ownPoints = ClassDataPoints();
    
    init(points : ClassDataPoints){
        for ele in points.points {
            m_ownPoints.points.append(ele.getCopy());
//            print("Point: \(m_ownPoints.points.last!)");
        }
    }
    
    func test(withData testPoints : DataPoints) -> ClassDataPoints {
        let newClassPoints = ClassDataPoints();
        for point in testPoints.points {
            var nearestClass = (m_ownPoints[0] as! ClassDataPoint).classNr;
            var smallestDist = point.getEucDist(to: m_ownPoints[0]);
//            print("New Dist : \(point.getEucDistSqrt(to: m_ownPoints[0]))");
            for i in 1 ..< m_ownPoints.count {
//                print("New Dist : \(point.getEucDistSqrt(to: m_ownPoints[i]))");
                let newDist = point.getEucDist(to: m_ownPoints[i])
                if newDist < smallestDist {
                    smallestDist = newDist;
                    nearestClass = i;
                }
            }
//            print("Smallest Dist: \(point.getEucDistSqrt(to: m_ownPoints[nearestClass]))");
//            print("N: \(nearestClass)")
            newClassPoints.append(ClassDataPoint(oldPoint: point, classNr: nearestClass));
            }
//        print("NewP : \(newClassPoints)")
        
        return newClassPoints;
    }
    
}