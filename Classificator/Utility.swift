//
//  Utility.swift
//  DynamicOctreeSpace
//
//  Created by Max on 16.10.15.
//  Copyright © 2015 Maximilian Denninger. All rights reserved.
//

import Foundation

let isDebug = true;

func printInfo( line : String, function : String = __FUNCTION__, file : String =  __FILE__, codeLine : Int = __LINE__) {
    var pathPrefix = NSURL(fileURLWithPath: file).lastPathComponent!;
    pathPrefix.removeRange(Range<String.Index>(start: pathPrefix.startIndex.advancedBy(pathPrefix.lengthOfBytesUsingEncoding(NSUTF8StringEncoding) - 6), end: pathPrefix.endIndex));
    let output1 = "In " + pathPrefix + "::";
    let output2 = function + "()::\(codeLine): " + line;
    print(output1 + output2);
}

func printDebug( line : String, function : String = __FUNCTION__, file : String =  __FILE__, codeLine : Int = __LINE__) {
    if(isDebug){
        var pathPrefix = NSURL(fileURLWithPath: file).lastPathComponent!;
        pathPrefix.removeRange(Range<String.Index>(start: pathPrefix.startIndex.advancedBy(pathPrefix.lengthOfBytesUsingEncoding(NSUTF8StringEncoding) - 6), end: pathPrefix.endIndex));
        let output1 = "In " + pathPrefix + "::";
        let output2 = function + "()::\(codeLine): " + line;
        print(output1 + output2);
    }
}

func printError( line : String, function : String = __FUNCTION__, file : String =  __FILE__, codeLine : Int = __LINE__) {
    var pathPrefix = NSURL(fileURLWithPath: file).lastPathComponent!;
    pathPrefix.removeRange(Range<String.Index>(start: pathPrefix.startIndex.advancedBy(pathPrefix.lengthOfBytesUsingEncoding(NSUTF8StringEncoding) - 6), end: pathPrefix.endIndex));
    let output1 = "Error in " + pathPrefix + "::";
    let output2 = function + "()::\(codeLine): " + line;
    print(output1 + output2);
}

func printErrorIf( line : String, isError : Bool, function : String = __FUNCTION__, file : String =  __FILE__, codeLine : Int = __LINE__) {
    if(isError){
        var pathPrefix = NSURL(fileURLWithPath: file).lastPathComponent!;
        pathPrefix.removeRange(Range<String.Index>(start: pathPrefix.startIndex.advancedBy(pathPrefix.lengthOfBytesUsingEncoding(NSUTF8StringEncoding) - 6), end: pathPrefix.endIndex));
        let output1 = "Error in " + pathPrefix + "::";
        let output2 = function + "()::\(codeLine): " + line;
        print(output1 + output2);
    }
}